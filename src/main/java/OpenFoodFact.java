import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import static org.apache.spark.sql.functions.*;

public class OpenFoodFact {
    public static void EtlUsingDataFrame() {
        SparkSession sparkSession = SparkSession.builder().appName("OpenFoodFact ETL").master("local[3]").getOrCreate();

        Dataset<Row> data = sparkSession.read().format("csv")
                .option("header", "true")
                .option("delimiter", "\\t")
                .load("C:/Users/Enzo/Desktop/4E année/Atelier données/openfoodfacts.csv");

        Dataset<Row> filteredData = data.filter(col("product_name").isNotNull().and(col("categories").isNotNull()));

        filteredData = filteredData.filter(expr("lower(categories) NOT LIKE '%boissons%'"));

        Dataset<Row> selectedData = filteredData.select(
                data.col("product_name"),
                data.col("categories"),
                data.col("energy_100g"),
                data.col("energy-kj_100g"),
                data.col("energy-kcal_100g"),
                data.col("proteins_100g"),
                data.col("carbohydrates_100g"),
                data.col("sugars_100g"),
                data.col("glucose_100g"),
                data.col("lactose_100g"),
                data.col("fat_100g"),
                data.col("saturated-fat_100g"),
                data.col("fiber_100g"),
                data.col("sodium_100g")
        );
        selectedData = selectedData.na().drop();
        selectedData.show();
    }

    public static void main(String[] args) {
        EtlUsingDataFrame();
    }
}
